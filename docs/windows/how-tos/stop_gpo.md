# Useful links
<https://www.remkoweijnen.nl/blog/2009/02/25/preventing-domain-group-policies-from-applying/>  
<https://www.remkoweijnen.nl/blog/2008/12/09/new-universal-patch-method/>

# Files
- [RunAsSys](../utils/runassys.md)
- dup2
- GroupPolicyClient.dUP2 - dup2 patch

# Notes
- RunAsSys => `services.msc` => Disable gpsvc (or via registry)
- Patch `gpsvc.dll` using dup2 or use admin account
