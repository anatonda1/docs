Windows 7 hotspot feature is for sharing your computers internet connection with other devices.  
You need to check if your network card has the latest driver to support the feature.  
Go to the **Device Manager** and look for the **Network Adapter** and check whether you have installed latest driver or not.

**Step 1:**  
Put the following commands into a .bat-file for simpler and faster usage:

- Open an elevated command prompt
- `netsh wlan set hostednetwork mode=allow ssid=MyNet key=MyPassword keyUsage=persistent`
- `netsh wlan start hostednetwork`

&nbsp;  
**Step 2:**  

- **Control Panel**
- **Network and Internet**
- **Network and Sharing Center**
- **Setup a new connection or network**
- In the **Setup a Connection or Network** window, under the **Choose a Connection** option, select **Set up a wireless ad hoc (computer-to-computer) network**
- It will ask you to enter the network name and security keys
- It will display the **Network Name** and **Key** of the newly formed wireless network
- The newly formed Wireless connection now appears under the **Wireless Network Connection** and is ready for users to connect. In order to enable the users to use it as a hotspot, you need to share your Internet Connection with them
- To share your internet connection, go to **Control Panel** -> **Network and Internet** -> **Network and Sharing Center** and click on **Change Adapter Settings**
- Now right click on the connection that enables you internet access and select **Properties**
- In the **Connection Properties** window, open the **Sharing** tab and check the **Allow other network users to connect through this computer’s internet connection**

At this point, you should be able to connect any wireless device to your new ad hoc network and access the internet or even share files directly with your laptop.

