# Useful links
<https://superuser.com/questions/1282409/find-corresponding-inf-file-for-device-driver>

# Notes
Drivers location:  
`C:\Windows\System32\DriverStore\FileRepository`

```powershell
# Win + R > devmgmt.msc
# Find device of interest
# Right click -> "Properties" -> “Details” -> "Property" -> "Inf Name"

dism /Online /get-drivers /format:table | Select-String -Pattern “oemXX.inf”

# Find original name in the driver location folder
```