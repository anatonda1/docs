# Useful links
<https://www.wisecleaner.com/how-to/119-how-to-turn-off-windows-defender-in-windows-10.html>  
<https://superuser.com/questions/1757339/how-to-permanently-disable-windows-defender-real-time-protection-with-gpo>  

# How-to
**In Powershell:**  
```powershell
Set-MpPreference -DisableRealtimeMonitoring 0
```
  
**Registry:**  

- Navigate through the tree to `HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows Defender`
    - Delete `DisableAntiSpyware` in the right pane.
- Navigate to `HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender\Real-Time Protection`
    - Delete `DisableRealtimeMonitoring` in the right pane.

- Go to `HKEY_LOCAL_MACHINE\SoftwarePolicies\Microsoft\Windows Defender`
    - Delete the value in the `DisableAntiSpyware` key.

&nbsp;  
More specifically, some users will see two options:

- Under type, it says – `REG -DWORD`
- Under Data, it says – `(invalid DWORD (32-bit) value)`
Double click on the `REG -DWORD` and a small window will open where you can delete the value or set it to zero. If you can’t change the key value, delete the whole key.  
Some users are reporting that you can fix the problem by deleting the `DisableRealtimeMonitoring` DWORD so you might want to try that as well.
