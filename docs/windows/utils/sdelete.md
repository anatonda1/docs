# Useful links
<https://docs.microsoft.com/en-us/sysinternals/downloads/sdelete>  
<https://serverfault.com/questions/165070/how-to-zero-fill-a-virtual-disks-free-space-on-windows-for-better-compression>


# Files
- SDelete161

# Notes
Use to zero-fill free space in order to save space when converting to VM  
Version 2+ has issues with zeroing, will get stuck on 100%, use 1.61 instead


# Commands
```powershell
# Zero-out drive C:
sdelete.exe -z c:
```
