# Useful links
<https://docs.microsoft.com/en-us/windows-server/networking/technologies/netsh/netsh-contexts>  
<https://lzone.de/cheat-sheet/netsh>  

# Commands
```powershell
# Show and remove wireless profiles
netsh wlan show profiles
netsh wlan delete profile name="profilename"
```
