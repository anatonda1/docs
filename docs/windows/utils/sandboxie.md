# Useful links
<https://sandboxie-website-archive.github.io/www.sandboxie.com/StartCommandLine.html>  
<https://sandboxie-plus.com/>


# Files
- Sandboxie-Classic-x64-v5.55.15


# Commands
```powershell
# Run program sandboxed in a specific sandbox
"C:\Program Files\Sandboxie\Start.exe" /box:<boxname> <program_name_or_path>

# Elevate permissions. Ask for UAC prompt if neccessary
"C:\Program Files\Sandboxie\Start.exe"  /elevate cmd.exe

# Environment variables
"C:\Program Files\Sandboxie\Start.exe"  /env:VariableName=VariableValueWithoutSpace
"C:\Program Files\Sandboxie\Start.exe"  /env:VariableName="Variable Value With Spaces"

# Hide program window until finished
"C:\Program Files\Sandboxie\Start.exe"  /hide_window cmd.exe /c automated_script.bat
```
