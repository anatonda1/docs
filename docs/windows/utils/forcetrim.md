# Useful links
<http://ticksontech.blogspot.com/2011/12/trim-for-masses_10.html>


# Files
- ForceTrim


# Notes
Windows does not have `fstrim` command as linux does. Tools are mostly vendor specific.  
This should work for all SSDs. Run as Administrator.  