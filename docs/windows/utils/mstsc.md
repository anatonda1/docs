# Useful links
<https://stackoverflow.com/a/35193691> - Disable output suppression (for mousejiggler or similar)  
<https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/rdp-files>


# Registry
Save code boxes as `.reg` files and run

Disable output suppression
```toml
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Microsoft\Terminal Server Client]
"RemoteDesktop_SuppressWhenMinimized"=dword:00000002
```

Disable connection bar pinning
```toml
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Microsoft\Terminal Server Client]
"PinConnectionBar"=dword:00000000
```
