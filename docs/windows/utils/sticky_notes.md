# Useful links
<https://www.geckoandfly.com/7902/how-to-change-windows-7-sticky-notes-font-size-and-style/>


# Notes
**NOTE:** To change the font, use word editors such as LO Writer or MS Word and paste formatted text into it  

`Ctrl+B` – Bold text  
`Ctrl+I` – Italic text  
`Ctrl+T` – Strikethrough  
`Ctrl+U` – Underlined text  
`Ctrl+Shift+L` – Bulleted (press once) or Numbered (press twice) list  
`Ctrl+Shift+>` – Increase text size  
`Ctrl+Shift+<` – Decrease text size  
`Ctrl+A` – Select all  
`Ctrl+Shift+A` – Toggles all caps  
`Ctrl+L` – Left aligns text  
`Ctrl+R` – Right aligns text  
`Ctrl+E` – Centers text  
`Ctrl+Shift+L` – Small Alpha list (3rd), Capital Alpha list (4th), small roman (5th), Capital roman (6th)  
`Ctrl+1` – Single-space lines  
`Ctrl+2` – Double-space lines  
`Ctrl+5` – Set 1.5-line spacing  
`Ctrl+=` – Subscript  
`Ctrl+Shift++` – Superscript  