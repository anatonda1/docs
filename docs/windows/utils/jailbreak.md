# Useful links
<https://github.com/iSECPartners/jailbreak>


# Files
- windows-jailbreak


# Notes
Use to export certificates marked as non-exportable in certmgr.msc  
Works for 64-bit Windows 7 - 10