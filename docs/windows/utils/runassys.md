# Useful links
<https://www.uwe-sieber.de/misc_tools_e.html>

# Files
- RunAsSystem

# Notes
```powershell
RunAsSystem [-nowait] [-min|-max|-normal|-noact|-hid] [-low|-below|-normal|-above|-high] "executable" [params for executable]
```

# Commands
```powershell
RunAsSystem "%windir%\regedit.exe"
RunAsSystem "%windir%\System32\cmd.exe" /k dir /s "C:\System Volume Information"
```
