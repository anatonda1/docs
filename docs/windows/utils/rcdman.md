# Useful links
<https://docs.microsoft.com/en-us/sysinternals/downloads/rdcman>


# Files
- RCDMan_283
- RCDMan_293

# Notes
- RCDMan 2.9 does not work on Windows 7
- Multimonitor setup is not supported on RCDMan < 2.83