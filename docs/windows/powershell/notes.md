```powershell
# Recursively get hash and filepath of each file in a specified folder
Get-ChildItem -Recurse "C:\Folder" | Get-FileHash | Select -Property Hash, Path | Format-Table -autosize

# Clear history (does not clear history file)
Clear-History # or Alt+F7 (if Get-Module PSReadLine >= 1.2)

# Path to saved history file
(Get-PSReadlineOption).HistorySavePath

# Disable history for running session
Set-PSReadLineOption -HistorySaveStyle SaveNothing
```