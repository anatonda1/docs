```powershell
function doStuff{
    # Change the location back to where the original script was as Admin's home folder is C:/Windows/System32
    Set-Location -Path $args[0]
    # TODO - rest of the script
}

# =======================
# Program starts here
# =======================

# Check if the script is running with admin priviledges
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
$areWeAdmin = $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

if($areWeAdmin -eq $false) {
    # If not, create a window for user to click
    Add-Type -AssemblyName PresentationFramework
    
    $msgText = "Installer must be run as Administrator. Would you like to continue? (UAC prompt might appear)"
    $msgTitle = "Administrator required"
    $msgButtons = "YesNo"
    $msgType = "Warning"
    $msgAnswer = [System.Windows.MessageBox]::Show("$msgText","$msgTitle","$msgButtons","$msgType")

    # Proper escaping, handles cases where there are spaces in file names or folders
    $filepath = Resolve-Path "this_script.ps1"
    $workpath = Resolve-Path "$pwd"

    if($msgAnswer -eq 'Yes') {
        Start-Process -FilePath "powershell" -Verb runas -ArgumentList @("-file `"$($filepath.path)`"","`"$($workpath.path)`"")
    } else {
        exit
    }
    
} else {
    # Already an admin, moving on
    doStuff $args[0]
}
```