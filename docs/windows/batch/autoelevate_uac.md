```dos
@echo off
setlocal enabledelayedexpansion

REM !!! Does not work if the path to the batch file contains spaces !!!
REM ===================================================
REM Elevate to administrator if necessary
REM ===================================================

net session >nul 2>&1
if %errorLevel% == 0 (
goto start
) else (
 powershell -command "Add-Type -AssemblyName PresentationFramework;[System.Windows.MessageBox]::Show('Installer must be run as Administrator. Would you like to continue? (UAC prompt might appear)','Administrator required','YesNo','Warning')" > %localappdata%\oculusoffline.tmp
set /p msgSelection=<%localappdata%\oculusoffline.tmp
del %localappdata%\oculusoffline.tmp

if !msgSelection! == Yes (
  powershell Start-Process -FilePath "%0" -ArgumentList "%cd%" -verb runas >NUL 2>&1
  exit /b
 ) else (
  exit
 )
)

:start
REM ========================
REM Stuff starts here
REM ========================

echo Running as admin
cd /d %1
echo %cd%
pause
```