# Useful links
<https://crontab.guru/>  

# Syntax

```bash
minute   hour  day(of month)  month  day(of week)    command
  *       *          *          *          *         /path/to/file

# Every 5 minutes
*/5 * * * * cd /path/to/script && ./script.sh

# Every 5 minutes from 8:00 to 23:59
*/5 8-23 * * * cd /path/to/script && ./script.sh

# At 8:05
5 8 * * * cd /path/to/script && ./script.sh

# Also possible to use variables
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

5 8 * * * echo $SHELL $PATH

```

# Commands

```bash
# Print crontab for current user
crontab -l

# Edit crontab for current user
crontab -e

# Modify system-wide crontab
# NOTE: System-wide cron requires an user to be specified under which to run the command
sudo nano /etc/crontab
--------------------------------------------------------------------
17 *   * * *   root   cd / && run-parts --report /etc/cron.hourly
.
.
.
... # More entries
--------------------------------------------------------------------

# It is also possible to add shell scripts to regularly executed folders
sudo nano /etc/cron.{hourly/daily/weekly/monthly}/script-name

```
