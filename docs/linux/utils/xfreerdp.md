# Useful links
<https://github.com/awakecoding/FreeRDP-Manuals/blob/master/User/FreeRDP-User-Manual.markdown>  
<https://stackoverflow.com/a/35193691> - Output suppression

# Commands
```bash
# Useful params
# Force span through all monitors
/multimon:force

/u:<username>
/d:<domain>
/v:<fqdn_or_ip>

# Use single quotes when special chars are involved
/p:'<password>'

# Play sound on remote machine
/audio-mode:1

/log-level:OFF

# Automatically try to reconnect
+auto-reconnect
/auto-reconnect-max-retries:20

# Negotiate network speed
/network:auto

# Graphics quality settings
/gfx:rfx
+gfx-progressive

# Disable output suppression
# Useful in case we use tools like mouse jiggler in order to prevent screen lock
# This makes sure that the tool will continue running in the background 
-suppress-output
```
