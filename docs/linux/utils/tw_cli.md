# Useful links
<https://serverfault.com/questions/525923/lsi-3ware-tw-cli-and-tdm2-segfault-with-debian-linux-kernels-after-3-8>


# Notes
To fix segfault after kernel 3.8

```bash
setarch x86_64 --uname-2.6 tw_cli /c0/u0 show all
linux64 --uname-2.6 tw_cli /c0/u0 show all
```