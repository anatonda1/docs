# Useful links
<https://www.openssl.org/docs/>

# Commands
```bash
# Encrypt to base64 (-a), output to a variable and decrypt
# -pass also accepts stdin
$SECRET=$(echo 'foo' | openssl aes-256-cbc -e -a -pbkdf2 -pass pass:<password>)
echo $SECRET | openssl aes-256-cbc -d -a -pbkdf2
```
