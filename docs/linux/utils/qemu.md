# Useful links
<https://qemu-project.gitlab.io/qemu/>  
<https://wiki.qemu.org/Main_Page>  
<https://wiki.gentoo.org/wiki/QEMU/Options>  
<https://wiki.qemu.org/Documentation/Networking>  


# Commands
```bash
# qemu-img
# =======================================
# Convert raw disk image -> vmdk
qemu-img convert -O vmdk disk.dd disk.vmdk

# Convert raw disk image -> qcow2
qemu-img convert -O qcow2 disk.dd disk.qcow2

# Shrink disk without compression
qemu-img convert -O qcow2 image.qcow2_backup image.qcow2

# Shrink disk with compression
qemu-img convert -O qcow2 -c image.qcow2_backup image.qcow2

# Disk info
qemu-img info image.qcow2

# Convert raw disk to qcow2
qemu-img convert -f raw -O qcow2 /path/to/your/hdd/vm01.img /path/to/your/hdd/vm01.qcow2
qemu-img convert -O qcow2 /dev/sda /path/to/image/sda.qcow2

# Extract qcow2 (even compressed) disk to a block device
qemu-img dd -f qcow2 -O raw bs=4M if=/path/to/image/image.qcow2 of=/dev/sda


# qemu monitor
# =======================================
# Mounting a USB device
# NOTE: option 'id=*' must be set in order to use "device_del" command. Use "info usbhost" for a list of VIDs/PIDs
device_add usb-host,vendorid=0x18d1,productid=0xd00d,id=usb1
device_del usb1

# Mountig an ISO file
# NOTE: drive with id 'cdrom0' must be present on the VM
# e.g use "-drive media=cdrom,id=cdrom0" parameter when running qemu-system command
info block
change cdrom0 /tmp/dsl-4.4.10.iso 
eject cdrom0
```

# qemu-system
All following paramters are to be specified after qemu-system-XXX command  
A parameter is specified by a dash (-) and it's options are separated by commas (,)  

## Machine
```bash
# For help with some parameters one can use:
qemu-system-XXX -<parameter_name> help
# Example:
qemu-system-x86_64 -device help

# Disable any default config -  run VM with specified parameters only
-nodefaults

# Disable graphical console
-nographic

# Add UEFI flash for EFI boot
# see https://www.kraxel.org/repos/jenkins/edk2/
-drive if=pflash,format=raw,readonly,file=efi.bin
-drive if=pflash,format=raw,file=efivars.bin

# Enable boot menu in firmware/BIOS/EFI (if supported)
-boot menu=on

# Pass through CPU info, disable KVM flag, 4C/2T, 20480MB RAM
-enable-kvm
-cpu host,kvm=off
-smp cores=4,threads=2
-m 20480

# Setup monitor on localhost
-monitor telnet:127.0.0.1:23,server,nowait 

# Attach XHCI (USB3.0) and SCSI hubs
-device qemu-xhci,id=xhci1
-device virtio-scsi-pci,id=scsi

# Attach e1000 NIC in user mode
# To disable DHCP, remove 'net' and 'dhcpstart' options
-netdev user,id=net0,net=192.168.76.0/24,dhcpstart=192.168.76.10
-device e1000,netdev=net0

# Attach cdrom
-drive media=cdrom,id=cdrom0 

# Attach qcow2 disk to appear as an SSD on SCSI bus inside the guest
# For async IO (aio=threads) param see https://specs.openstack.org/openstack/nova-specs/specs/mitaka/implemented/libvirt-aio-mode.html
-drive file=win10.qcow2,id=disk0,format=qcow2,if=none,discard=unmap,aio=threads
-device scsi-hd,drive=disk0

# Attach physical disk to appear as SSD on SCSI bus inside the guest
-drive file=/dev/disk/by-id/ata-Patriot_P200_512GB_AA000000000000000765,id=disk2,format=raw,if=none,discard=unmap
-device scsi-hd,drive=disk2 
```

## Samba fileshare
```bash
# Requires samba server to be installed on host system
# Does not provide DHCP, interface inside the guest must be configured manually
-netdev user,id=smb0,restrict=on,smbserver=10.0.3.4,net=10.0.3.0/24,smb=/path/to/host/folder
-device e1000,netdev=smb0
```

## Keyboard/Mouse
```bash
# Pass through physical mouse and keyboard, usefull for consoleless systems (GPU passthrough with output to a physical monitor)
# Use CTRL+CTRL (hard-coded) to release back to host system (grab_all=on,repeat=on)
-object input-linux,id=kbd0,evdev=/dev/input/by-id/usb-HP_HP_Wireless_KB_and_MS-event-kbd,grab_all=on,repeat=on 
-object input-linux,id=mouse0,evdev=/dev/input/by-id/usb-Logitech_USB_Receiver-if02-event-mouse 

# Add virtio keyboard/mouse controllers (requires virtio drivers)
-device virtio-keyboard-pci,id=kbd0
-device virtio-mouse-pci,id=mouse0
```

## VFIO
```bash
# Pass through device on PCI address 01:00.0 (GPU with audio in this case)
# Physical device must be already binded to vfio-pci driver on the host system
# If the device has multiple subdevices, add multifunction=on option to the main device
-device vfio-pci,host=01:00.0,multifunction=on
-device vfio-pci,host=01:00.1
```
