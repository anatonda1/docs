# Useful links
<https://wiki.archlinux.org/title/NetworkManager>

# Notes
Additional packages are usually needed for GUI integration, see <https://wiki.archlinux.org/title/NetworkManager#VPN_support>

# Commands
```bash
# Show interface properties
nmcli connection show <ConnectionName>

# Add persistence to VPN connection
nmcli connection modify <Your VPN connection name> connection.autoconnect-retries 0
nmcli connection modify <Your VPN connection name> vpn.persistent yes
```
