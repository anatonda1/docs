# Useful links
<https://github.com/yt-dlp/yt-dlp>  
<https://docs.yt-dlp.org/en/latest/README.html>

# Notes
netrc file example:
```
machine <extractor> login <username> password <password>
```

# Commands
```bash
# List extractors
yt-dlp --list-extractors

# Video details
yt-dlp --list-formats <URL>

# Download from youtube
yt-dlp --write-auto-sub --write-sub --sub-lang en -f 'bestvideo[height<=1080]+bestaudio' <URL>

# youtube - audio only + playlist
yt-dlp -f bestaudio -x --audio-format mp3 https://www.youtube.com/playlist?list=XXX

# Download from nebula
yt-dlp --netrc --netrc-location ./netrc.txt --write-auto-sub --write-sub --sub-lang en -f '[height<=1080]' \
-o "/path/to/dir/%(title)s [%(id)s].%(ext)s" <URL>

## download audio only (convert to mp3)
## use --audio-quality 0-10 for VBR or value (128k/320k) for CBR
yt-dlp -f bestaudio <URL>
yt-dlp -f bestaudio -x --audio-format mp3 <URL>

```
