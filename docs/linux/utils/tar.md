# Useful links
<https://neverendingsecurity.wordpress.com/2015/04/13/linux-tar-commands-cheatsheet/> 

# Commands
```bash
# List archive contents
tar -tzf name-of-archive.tar.gz

# Compres to .gz
tar -czvf name-of-archive.tar.gz /path/to/directory-or-file

# Compress to .bz2
tar -cjvf name-of-archive.tar.bz2 /path/to/directory-or-file

# Compress to .xz
tar -cJvf name-of-archive.tar.xz /path/to/directory-or-file

# Compress to .xz with optional parameters
XZ_OPT="-7 --threads=6" tar -cJvf name-of-archive.tar.xz /path/to/directory-or-file

# Extract archive to current directory
tar -xzvf archive.tar.gz
tar -xjvf archive.tar.bz2
tar -xJvf archive.tar.xz

# Possible to autodetect with newer versions
tar -xvf archive.tar.gz

# Extract archive to specific directory
tar -xvf archive.tar.gz -C /destination-dir
tar -xvf archive.tar.gz --directory /destination-dir
```
