# Useful links
<https://trac.ffmpeg.org/wiki/>  
<https://trac.ffmpeg.org/wiki/ExtractSubtitles>


# Commands
```bash
# Media info
ffmpeg -i <my_file>

# Export audio from source
ffmpeg -i VIDEO.mp4  -vn -acodec copy AUDIO.aac

# Mux audio and video without a video reencode
ffmpeg -i VIDEO.mp4 -i AUDIO.aac -c:v copy -b:a 320k OUTPUT.mp4

# Cut video without reencode (seek 30s + 10s)
ffmpeg -ss 00:00:30.0 -i VIDEO.mp4 -c copy -t 00:00:10.0 OUTPUT.MP4

# Extract subtitles
ffmpeg -i my_file.mkv outfile.vtt
ffmpeg -i my_file.mkv -f webvtt outfile
ffmpeg -txt_format text -i input_file out.srt
```
