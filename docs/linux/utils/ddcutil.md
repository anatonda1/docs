# Useful links
TODO

# Commands

```bash
# 100%
sudo ddcutil setvcp --display 1 10 100 && sudo ddcutil setvcp --display 2 10 100 && echo "100p"

# 66%
sudo ddcutil setvcp --display 1 10 66 && sudo ddcutil setvcp --display 2 10 66 && echo "66p"

# 1%
sudo ddcutil setvcp --display 1 10 1 && sudo ddcutil setvcp --display 2 10 1 && echo "1p"
```
