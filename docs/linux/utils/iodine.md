# Useful links
<https://www.root.cz/clanky/tunelujeme-provoz-pomoci-dns-cesta-ven-ze-site/>

**OPTIONAL:** Create a domain and 2 additional domain entries.  
Create an additional subdomain and let it be managed by a nameserver which will have the iodined daemon running on it

```dns
# BIND9 Style zone file entries
t1   3600 IN NS ns.domain.tld.    ;tunnel.domain.tld subdomain entries are managed by ns.domain.tld
ns   600  IN A  <IPADDR>          ;here runs iodined
```

# Commands
```bash
# Server
iodined -DDD -fP <password> 10.0.0.1 t1.domain.tld

# Client / specify subdomain nameserver / specify subdomain nameserver IP (if DNS(53) is unavailable, see iptables part)
iodine -fP <password> t1.domain.tld
iodine -fP <password> ns.domain.tld t1.domain.tld
iodine -fP <password> <ns.domain.tld_ipaddr> t1.domain.tld

# Testing
nc -z -v -u -n <IPADDR> <PORT>

# If the iodined does not run on standard port 53 you can use iptables on client
iptables -t nat -A OUTPUT -p udp --destination <ns.domain.tld_ipaddr> --dport 53 -j DNAT --to :<NEW_PORT>
# And don't forget to wipe afterwards
iptables -t nat --flush
```
