# Useful links
<https://blog.benjojo.co.uk/post/imaging-mounted-disk-volumes-live>  
<https://github.com/benjojo/hot-clone>


# Files
- hot-clone.tar.xz (Build date: 22.9.2021)

# Notes
Build requires go 1.16+


# Commands
```bash
# Clone disk to a file
sudo ./hot-clone -device /dev/sdb > sdb.hc

# Convert cloned disk to RAW/dd format
./hot-clone -reassemble sdb.hc -reassemble-output sdb.img
```