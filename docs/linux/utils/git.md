# Useful links
<https://rogerdudler.github.io/git-guide/>


# Commands
```bash
# Shallow clone
git clone <URL> --depth=1

# Clone a branch
git clone <URL> --branch=<BRANCH_NAME>
git clone <URL> -b <BRANCH_NAME>

# Change a branch
git checkout -b <BRANCH_NAME>

# Create and commit to a new remote branch
# -u = --set-upstream
git add .
git commit
git push -u origin <BRANCH_NAME>

# Amend changes (replace commit)
git status
git add .
git commit --amend --no-edit
## ALWAYS CHECK THE CURRENT BRANCH!!!
git push --force 

```
