# Useful links
<https://linux.die.net/man/8/ntfsprogs>  
<https://www.erol.name/ntfsclone-windows-partition-to-smaller-partition-on-another-disk/>

# Notes
NTFS is a finicky filesystem and ntfsprogs only care about filesystem consistency.  
Booting or partition alignement are out of scope, therefore user must take care of these when using the sofware to properly clone a bootable Windows disk.  


# Commands
```bash
# /dev/sdX - source disk
# /dev/sdY - target disk

# To properly clone a boot disk, partition alignement must be preserved
dd if=/dev/sdX of=MBR.dd bs=8M status=progress count=1
dd if=MBR.dd of=/dev/sdY status=progress

# UEFI
# TODO

# When cloning a boot disk, it's good idea to just transfer boot and system partitions directly
# First 2GB should be enough for Windows, but check beforehand
dd if=/dev/sdX of=/dev/sdY bs=1M status=progress count=2048

# Clone partitions
ntfsclone --force --overwrite <dest> <source>

# Resize a partition (source partition must be shrinked in order to clone to a smaller destination partition)
# All neccessary file relocations are handled automatically by ntfsresize
ntfsresize --size 32G /dev/sdX2
```
