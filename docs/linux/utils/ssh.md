# Useful links
<https://cheatsheet.dennyzhang.com/cheatsheet-ssh-a4>

# Notes

## ~/.ssh/config
```bash
Host mac
    Hostname mac.local

Host mac mac.local
    User myuser

# Default
Host *
    User myuser

# Proxy 
Host 10.200.0.*
    ProxyJump 10.11.22.33
```

# Commands
```bash
# Specify login
ssh <host> -l <username>

# Dynamic port forward (SOCKSv5)
ssh <host> -l <username> -D 5555

# Local port forward
ssh <host> -l username -L 443:<host>:443

# X forwarding
ssh <host> -l username -X

# export public key from private
ssh-keygen -y -f /path/to/private/key

# Use proxy already running on localhost
ssh -o ProxyCommand="nc -x localhost:8888 %h %p" my-server

# Do not add entries to .ssh/known_hosts ...
ssh -o UserKnownHostsFile=/dev/null -l <username> <host>
# ... or use hashing
echo 'HashKnownHosts yes' >> /etc/ssh/ssh_config
```
