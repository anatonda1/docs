# Useful links
<https://www.howtoforge.com/tutorial/understanding-7z-command-switches/>

# Commands
```bash
# Create password-protected archive without compression split into 500mb volumes
7z a archive -m00=Copy -v500m /path/to/file

# Password-protected, header encrypted archive without compression
7z a archiveName -mx=0 -pMyPassword -mhe=on /path/to/folder/
```
