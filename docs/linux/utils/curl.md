# Useful links
TODO

# Commands

```bash
# Follow redirects
curl -L <URL>

# Output to a file
curl -O <URL>

# Check supported TLS version
curl --verbose --tlsv1.2 --tls-max 1.2 <URL>
```
