# Useful links
<https://github.com/erebe/wstunnel>  
<https://www.root.cz/clanky/websocket-jako-cesta-k-uniku-z-prilis-restriktivni-site/>

# Overview
Useful when the only available protocol on a restricted network is http(s)

# Commands
```bash
# Server setup
wstunnel --server wss://0.0.0.0:443
wstunnel --server wss://0.0.0.0:443 -r 127.0.0.1:22

# Client setup
wstunnel -D 8888 wss://myRemoteHost:443
wstunnel -L 2222:127.0.0.1:22 wss://myRemoteHost:443

# Use as proxy with ssh
ssh -o ProxyCommand="wstunnel -L stdio:%h:%p wss://localhost:8888" my-server

# Connect via corporate proxy
wstunnel -L 9999:127.0.0.1:22 -p mycorporateproxy:8080 wss://myRemoteHost:443
```
