# Useful links
<https://cheatography.com/richardjh/cheat-sheets/rsync/>

# Commands
```bash
# Archive the filesystem to another mountpoint
rsync -aAXv / --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} /mnt
```