# Useful links
<https://devhints.io/find>

# Commands
```bash
# Find files older than 24 hours (in minutes)
find . -type f -mmin +1440

# Remove files older than 24 hours
find . -type f -mmin +1440 -exec rm -rf {} \;

# Diff 2 directories
find directory1 -printf "%P\n" | sort > file1 && find directory2 -printf "%P\n" | sort | diff - file1
```
