# Commands
```bash
# To disable mitigations in order to gain performance
nano /etc/default/grub

# Kernels older than 5.1
GRUB_CMDLINE_LINUX="noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off mitigations=off"

# Since kernel 5.1
GRUB_CMDLINE_LINUX="mitigations=off"

# Update grub configuration
update-grub

# Reboot and check the status
cat /sys/devices/system/cpu/vulnerabilities/*

KVM: Vulnerable
Mitigation: PTE Inversion; VMX: vulnerable
Vulnerable; SMT vulnerable
Vulnerable
Vulnerable
Vulnerable: __user pointer sanitization and usercopy barriers only; no swapgs barriers
Vulnerable, IBPB: disabled, STIBP: disabled
Vulnerable
Not affected
```
