# Useful links
<https://wiki.archlinux.org/title/sparse_file>


# Overview
On Unix based system is possible and to use files as a raw virtual harddisks.  
On most modern filesystems, sparse files are also supported so there's usually no need to create huge zero-filled files  

**Note:** It's also possible to create a single disk partition by formatting the file directly ( `mkfs.ext4 ./file.img` ), in which case the file can be mounted directly using `mount` command.


# Commands
```bash
# Create a sparse file to be used as a backing file
truncate -s 512M file.img
# or using dd
dd if=/dev/zero of=file.img bs=1 count=0 seek=512M

# Alternatively, if sparse files are not supported, create a zero-filled file of desired disk size
dd if=/dev/zero of=file.img bs=1M count=8192

# Create partitions
fdisk ./file.img
...

# Mount the file as a loop device (/dev/loopX)
losetup --find --show ./file.img

# Refresh partition table if neccessary
partprobe /dev/loopX

# Create a filesystem on the partition(s)
mkfs.ext4 /dev/loopXpY

# To detach the loop device
losetup -d /dev/loopX
```
