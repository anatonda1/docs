# Useful links
<https://forums.centos.org/viewtopic.php?t=63988>


# Overview
![virtualbox](../01_img/virtualbox.png)  

This error usually occurs when moving VMs between different virtualization solutions for example QEMU -> VMware  
while the system is installed on a /dev/mapper device (LVM/mdraid).  

Kernel needs to be reinstalled and initramfs re-generated.  

# Commands
```bash
# 1) Boot into rescue mode
# 2) Ctrl+Alt+F2
# 3) Login as root

# 4a) Option 1:
yum update kernel
# or
yum reinstall kernel-X.X.X

# 4b) Option 2:
# TODO
```
