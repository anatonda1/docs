# Useful links
<https://askubuntu.com/questions/1089460/how-do-i-uninstall-the-running-kernel-by-command-only>

# Overview
Tested on Ubuntu 20.04  
Also applies to Debian 10+  

By default the Debian/Ubuntu does not allow users to non-interactively remove kernel package of a kernel that is currently running.  
This causes issues with automation where the user input is not possible or desirable.  

**Note:** You must install a different kernel on the system, either prior to or after the removal, otherwise the system will not boot (duh!).

# Commands
```bash
# Remove the check
mv /usr/bin/linux-check-removal /usr/bin/linux-check-removal.orig
echo -e '#!/bin/sh\necho "Overriding default linux-check-removal script!"\nexit 0' | tee /usr/bin/linux-check-removal
chmod +x /usr/bin/linux-check-removal

# Remove running kernel
apt-get remove --purge <kernel-packages>

# Install different kernel and reconfigure if neccessary (initrd, modules, etc.)
apt-get install <kernel>
dpkg-reconfigure linux-image-<kernelver>

# Restore check
mv /usr/bin/linux-check-removal.orig /usr/bin/linux-check-removal
```
