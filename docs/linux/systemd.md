# Useful links
<https://wiki.archlinux.org/title/systemd>  
<https://www.computernetworkingnotes.com/linux-tutorials/systemd-units-explained-with-types-and-states.html>  
<https://unix.stackexchange.com/a/367237> - Where do I put my systemd unit file?  
<https://www.shellhacks.com/systemd-service-file-example>  
<https://vsupalov.com/django-systemd-crashcourse/>  
<https://www.root.cz/clanky/sandboxing-se-systemd-zesileni-ochrany-sluzeb-pomoci-namespaces/>  
<https://askubuntu.com/questions/816285/what-is-the-difference-between-systemctl-mask-and-systemctl-disable>  

# Notes
## systemctl list-units vs list-unit-files:
- `list-units` - snapshot of currently running system
- `list-unit-files` - display status of units at startup

## unit states:
- `static` - cannot be enabled/disabled via systemctl, usually missing `[Install]` section in the unit file

## Other:
- `/usr/lib/systemd` - default units provided by the system and installed by packages
- `/etc/systemd/system/` - custom units
- `/etc/systemd/journald.conf` - journald configuration
- `/var/log/journal` - journald logs (use `--vacuum-XXXX` parameter to clean)

In `[Service]` section, if `ExecStart(Pre/Post)` command is prepended by `-` it means that the command may fail and the service will not be considered failed.  

Unit files must contain full paths to binaries when using ExecStart  
Systemd is not aware of any `PATH` variables during the startup unless explicitly specified in the service file  


# Commands
## systemctl
```bash
# Show system status
systemctl status

# enable/disable services
systemctl enable <unitname>
systemctl disable <unitname>

# List all services
systemctl list-units --type=service --state=running

# List all running services
systemctl list-units --state=running

# Reload unit configuration and regenerate dependencies tree
# Note: daemon-reload does not restart the services, this must be done manually if/as neccessary
systemctl daemon-reload

# Show unit file
systemctl cat <unitname>

# Modify unit configuration (e.g. env vars) or service file itself
systemctl edit <unitname>
systemctl edit --full <unitname>

# Get default target
systemctl get-default

# Set default target
systemctl set-default <targetname>

# Change target
systmectl isolate <targetname>

# Get dependency tree
systemctl list-dependencies
systemctl list-dependencies --all
systemctl list-dependencies --user
systemctl list-dependencies --system
systemctl list-dependencies <target>
systemctl list-dependencies <service>
```

## journalctl
```bash
# List all log entries
journalctl

# Log entries for current boot
journalctl -b

# and previous boot
journalctl -b -1 (-2,-3, ... -n)
journalctl -b <bootID>

# List boots (with IDs)
journalctl --list-boots

# List logs since
journalctl --since yesterday
journalctl --since "2015-01-10 17:15:00"
journalctl --since "2015-01-10" --until "2015-01-11 03:00"
journalctl --since 09:00 --until "1 hour ago"

# List log entries for a given service or UID
journalctl -u nginx.service
journalctl -u nginx.service --since today

journalctl _UID=33 --since today

# List kernel log
journalctl -k

# List messages by priority
journalctl -p err

# Output to stdout
journalctl --no-pager

# Output format
journalctl -o json
journalctl -o verbose

# Print only n last entries
journalctl -n XX

# Follow log
journalctl -f

# Disk usage
journalctl --disk-usage

# Cleanup
journalctl --vacuum-size 1G
journalctl --vacuum-time 1month
```


## hostnamectl
```bash
# Hostname and system info
hostnamectl

# Configure hostname
# Note: Standard is to use short name instead of FQDN 
hostnamectl set-hostname --pretty "<hostname>"
hostnamectl set-hostname --static "<hostname>"
hostnamectl set-hostname --transient "<hostname>"
```

## resolvectl
```bash
# DNS info
resolvectl
resolvectl statistics
```

## timedatectl
```bash
# Show status
timedatectl status

# List timezones
timedatectl list-timezones

# Set timezone
timedatectl set-timezone "Europe/Prague"

# Set time manually
# Active NTP overrides this
timedatectl set-time HH:MM:SS

# Set HW clock to local/UTC
timedatectl set-local-rtc 1
timedatectl set-local-rtc 0

# NTP
# =============
# Note: Requires timesyncd service to be present and running
# Enable NTP
timedatectl set-ntp true

# Configure NTP
nano /etc/systemd/timesyncd.conf
systemctl restart systemd-timesyncd.service
```


# Unit files
## Example
```toml
[Unit]
Description=Sleep service
After=network.target

[Service]
Environment="TIME=600" "STARTMSG=starting" "ENDMSG=ending"
ExecStart=/bin/sleep $TIME
ExecPre=/bin/echo $STARTMSG
ExecPost=/bin/echo $ENDMSG

[Install]
WantedBy=multi-user.target
```
