# Notes
## Config file
```yaml
# Old
network:
  version: 2
  renderer: networkd
  ethernets:
    <iface>:
     dhcp4: no
     addresses: [192.168.1.222/24]
     gateway4: 192.168.1.1
     nameservers:
       addresses: [8.8.8.8,8.8.4.4]
       
# New
network:
  version: 2
  renderer: networkd
  ethernets:
    <iface>:
      dhcp4: no
      addresses:
        - 192.168.1.247/24
      nameservers:
        addresses: [4.2.2.2, 8.8.8.8]
      routes:
        - to: default
          via: 192.168.1.1
```

# Commands
```bash
netplan try
netplan --debug apply
```

