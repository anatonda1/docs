# Useful links
<https://wiki.archlinux.org/title/Cgroups>  
<https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/ch01>  

**Systemd**  
<https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/resource_management_guide/chap-introduction_to_control_groups> (systemd)  
<https://systemd.io/CGROUP_DELEGATION/>  
<https://systemd.io/TRANSIENT-SETTINGS/>  
<https://man7.org/linux/man-pages/man5/systemd.resource-control.5.html> - systemd resource params  

# Overview
cgroups can be generally managed by 3 approaches:

- systemd (`systemctl`, `systemd-cg*`)
- libcgroup
- directly via virtual filesystem (`/sys/`)

There are historically 2 cgroup versions:

- cgroup - controlled via libcgroup or via sysfs
- cgroup v2 - controlled primarily by systemd and it's utilities

The main difference is the unification of hierarchical trees into a single tree

One can switch between these by setting a kernel parameter in `/etc/default/grub`  
`GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=1"` or `"0"`

# Rules
- Processes are called tasks
- Tasks are attached to hierarchies
- Each task can only be a member of one cgroup in a hierarchy
- Hierarchies can have one or more subsystems attached but these are then exclusive to that hierarchy  
  **NOTE:** This applies to v1, v2 uses single unified hierarchy with all subsystems included
- Subsystems represent resources (these are also known as (resource)controllers) such as:
    - cpu
    - blkio
    - devices
    - memory
    - ns
    - etc ...
- All child tasks forked from parent inherit their cgroup membership
- After the fork is done, child and parent tasks are independend of each other and can change cgroup membership independently

# systemd
Systemd uses slices to define constraints, the root slice is called `-.slice ` 
Service, scope, and slice units directly map to objects in the cgroup tree  

System wide resource constraints can be set by modifying `/etc/systemd/system.conf`  
Unit files can be configured separately using `[Service]` section in the unit's file configuration  

There are 2 types of cgroups that can be used with systemd:

1. Transient (present only during runtime, start a unit with `systemd-run`)
2. Persistent (requires modification to unit file)

# Commands
```bash
# Monitoring
systemd-cgls
systemd-cgls <controller>
systemd-cgls cpu/memory/etc

systemd-cgtop
systemctl -t slice

# List current active subsystems/controllers
mount | grep cgroup

# Run a command in a specified cgroup
# This will create a <name>.scope unit which can be managed by systemctl
systemd-run --unit=<name> --scope --slice=<slice_name> command

# Persistently set configuration via systemctl
systemctl set-property <unitname> parameter=value
systemctl set-property httpd.service CPUShares=600 MemoryLimit=500M

# Transiently set configuration via systemctl
systemctl set-property --runtime <unitname> property=value

# List what cgroups are assigned to a task
cat /proc/<PID>/cgroup
```

# Unit files
## Slice unit example
create or edit `/etc/systemd/system/my.slice`  

```toml
[Slice]
CPUQuota=30%
```

## Service unit example
Either run `systemctl edit <unitname>` or create `*.conf` file in `/etc/systemd/system/<unitname>.d/`  

```toml
[Service]
Slice=my.slice

# Limit service to 1 gigabyte
MemoryMax=1G

# Setting any cgroup parameter not yet directly supported by systemd
ExecStartPost=/bin/bash -c "echo 1G > /sys/fs/cgroup/memory/system.slice/<unitname>.service/memory.memsw.limit_in_bytes"
```
