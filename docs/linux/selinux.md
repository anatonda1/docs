# Useful links
<https://www.computernetworkingnotes.com/linux-tutorials/selinux-explained-with-examples-in-easy-language.html>  
<https://www.computernetworkingnotes.com/linux-tutorials/troubleshooting-selinux-explained-with-booleans.html>  
<https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_selinux/index>  
<https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_selinux/writing-a-custom-selinux-policy_using-selinux>  

# Notes
TODO - Read the redhat doku

# Commands
```bash
# Status
getenforce
sestatus

# Disable selinux
setenforce 0

# Disable selinux persistently
nano /etc/selinux/config

SELINUX=disabled

```
