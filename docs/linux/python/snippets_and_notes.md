```python
# Escaping chars
# ======================================================================
# Use either “ ” or \ before the character to be escaped

system("echo " + str(responseSell) + "|" + "tr ',' \"\\n\" | " + "grep -e pair -e cost -e fee -e ' type' -e time | " + "cut -d : -f 2 | " + "tr \" '\" '\\0' | " + "sed '0~5 a\\\\'")


# Dict to lists
# ======================================================================
result = {'key1' : 'value1'}
list1 = list(result.keys())
list2 = list(result.values())


# Switch case
# ======================================================================
def case(name):
    switcher = {
        '1': "Reply1",
        '2': "Reply2",
        '3': "Reply3",
        '4': "Reply4",
    }
    return switcher.get(name)


# Switch case with functions
# ======================================================================
def selector(options):
    # Lambda is used to prevent execution of functions when
    # the dictionary is created
    submodule = options['submodule']

    selector = {
        'get_rates'      : lambda : get_rates.run(options),
        'buy'            : lambda : buy.run(options),
        'sell'           : lambda : sell.run(options),
        'graphs_current' : lambda : generate_graphs_current.run(options),
    }

    selectedFunc = selector.get(submodule, lambda: 'Invalid')
    selectedFunc()


# Ask for input
# ======================================================================
choice = input("> ")


# Arguments check
# ======================================================================
# TODO: Parametrize # of arguments

from sys import argv

arguments = []
for arg in argv:
    arguments.append(arg)

if len(arguments) > 2:
    print(f'Too many arguments - exiting')
    exit()

if len(arguments) == 2:
    if arguments[1] == 'some_argument':
        print(f'{arguments[1]} provided')
        
    else:
        print('Unknown argument - exiting')
        exit()
else:
    print('No argument provided')


# Assign process output to a variable
# ======================================================================
from os import popen

currentIP = popen('curl -s ifconfig.me/ip').read()


# List to string (comma separated)
# ======================================================================
stringToCreate = ','.join([str(elem) for elem in myArray])


# Print error message
# ======================================================================
try:
    something
except ValueError as e:
    errMsg = str(e)
    print(f"{errMsg}")
    
    
# Calling function name as a variable
# ======================================================================
# https://stackoverflow.com/questions/12693606/reason-for-globals-in-python/18965612#18965612

def functionABC():
    print(f'This is function ABC')

def functionDEF():
    print(f'This is function DEF')

var = 'ABC'
var2 = 'DEF'

globals()['function' + var]()
globals()['function' + var2]()


# Import python file with name specified as a variable
# https://stackoverflow.com/questions/17136772/eval-to-import-a-module
# ======================================================================
# python file should be located in ./modules/some_module/api.py relative
# to main python executable
import importlib

module_name = 'some_module'

apiHandle = importlib.import_module('modules.' + module_name + '.api')
paramsValidation = apiHandle.validateParams(pairAsList, resolution)


# Import all modules in folder and subfolders
# (__init__.py required in each folder)
# ======================================================================
import importlib
import pkgutil

for mod_info in pkgutil.walk_packages(__path__, __name__ + '.'):
    mod = importlib.import_module(mod_info.name)

    # Emulate `from mod import *`
    try:
        names = mod.__dict__['__all__']
    except KeyError:
        names = [k for k in mod.__dict__ if not k.startswith('_')]

    globals().update({k: getattr(mod, k) for k in names})
    

# Lock time with system clock to repeat every N seconds
# Does not create a drift
# ======================================================================
import time

starttime = time.time()
while True:
    print "tick"
    time.sleep(60.0 - ((time.time() - starttime) % 60.0)) 
 

# Recursively search through JSON and return value for a given key (all levels)
# ======================================================================
def jsonSearch(dictionary, string):
    for key, value in dictionary.items():
        if key == string:
            yield value
        elif isinstance(value, dict):
            for val in jsonSearch(value, string):
                yield val


# Run multiple processes and wait until they all finish
# using subprocess.Popen
# ======================================================================
import subprocess

processes = []

for process in processesToSpawn:
    args = ['executable', arg1, arg2, ...]
    # With STDOUT to DEVNULL
    p = subprocess.Popen(args, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    processes.append(p)

[process.communicate() for process in processes]

```