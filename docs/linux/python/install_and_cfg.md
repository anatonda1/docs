```bash
# Python 3.7 on Ubuntu 18.04
# ======================================================================
apt install python3.7 python3-venv python3-dev

# Check installed versions and currently used version
ll /usr/bin/python*
python3 -V

# If 3.7 is not the default, use update-alternatives to configure
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
sudo update-alternatives --config python3

# Check
python3 -V
Python 3.7.5


# Venv
# ======================================================================
python3 -m venv <envname>

# Use via activate
source envname/bin/activate
pip3 install <package>
deactivate

# Alternatively use venv's python executable directly
./env/bin/python3 -m pip install <package>
./env/bin/python3 ./main.py

```