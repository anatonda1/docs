# IP Checker
Requires sendmail and curl

```python
# IP Checker (sendmail)
# NOTE: For cron usage: chmod ug+x and use absolute path to script
# ======================================================================
#!/usr/bin/python3

from os import popen, system

# =====================================
# Vars - modify as neccessary

enableEmailNotifications = 1

ipfile = 'ipchecker_ip.txt'

# =====================================

def sendEmail():
    system('echo "Subject: IP Change detected" | cat - ' + ipfile + ' | /usr/sbin/sendmail -f source@mail.com destination@mail.com')

try:
    f = open(ipfile, 'r')
except FileNotFoundError:
    f = open(ipfile, 'w+')
    f.write('333.0.0.1') # use bogus IP
    f.close()

f = open(ipfile, 'r')
storedIP = f.read().splitlines()[0]
f.close()

currentIP = str(popen('curl -s ifconfig.me/ip').read())

if currentIP != storedIP:
    f = open(ipfile, 'w')
    f.write(currentIP + "\n")
    f.close()

    if enableEmailNotifications == 1:
        sendEmail()
        
```
