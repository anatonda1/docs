# Useful links
<https://bash.cyberciti.biz/guide/$IFS>  
<https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/>  

# Notes
In general, it is recommended to use safeguards via “set” builtin when writing bash scripts, these prevent the script from running wild in the system.  
Set these at the start of every bash script!


```bash
# Safeguards
# -e - exit on error, use " || true" where failure is acceptable (e.g. "if" statemens)
# -u - check for unset variables and exit if found
# -x - verbose output (print all commands)
# -o pipefail - prevent piping from causing non-zero exit codes to be ignored
# -E (mandatory with ERR traps) - make ERR traps inheritable by functions, subshells and command substitutions

set -euxo pipefail
```

# Code & Snippets

## Arguments
```bash
# Arguments check
if [ $# -ne 2 ]
  then
    echo "Specify 2 arguments"
    exit
fi

# Check if file exists
if [ ! -f $1 ]; then
    echo "File not found!"
    exit
fi

# Check if argument is a number, via regex
re='^[0-9]+$'
if ! [[ $2 =~ $re ]] ; then
   echo "Second argument is not a number"
   exit
fi
```

## Process management
```bash
# Spawn processes in the background and wait for them to finish
processCount=5

for (( i=0; i<$processCount; i++ ))
do
    sleep 900 &
done

echo "Waiting to finish"
wait
```

## Escaping
```bash
# sed - print lines between 10 and 20 to a file
start=10
stop=20
sed -n ''"$start"','"$stop"'p;'"$(($stop+1))"'q' filename.txt > out.txt

## expanded command looks like this:
sed -n '10,20p,21q' filename.txt > out.txt

# To keep newlines intact, use "" with a variable
FILES=$(find . -type f)
echo "Found $(echo "$FILES" | wc -l) files"
```

## Other
```bash
# Pass keypresses to a program
(echo n; echo p; echo 1; echo 1; echo 200; echo w) | fdisk /dev/sda

# Merge 2 arrays together
ARRAY=("${ARRAY1[@]}" "${ARRAY2[@]}")

# Check if variable is empty
if [ -z "$var" ]
then
      echo "\$var is empty"
else
      echo "\$var is NOT empty"
fi
```

## One liners
```bash
# Check if file does not exist
# use -b for block device, -d for directory
if [ ! -f /path/to/file ]; then echo "file does not exist"; else echo "file exists"; fi

# Print folder contents every 5 seconds
while true; do clear && date && ls -ls && sleep 5; done
```

## Loops
```bash
# For loop
# IFS must be set to a newline as for loop splits the strings into an array using spaces as the default
IFS=$'\n'
FILES=$(find $VIDDIR -type f -mmin $MTIME)

for file in $FILES
do
    echo $file
done
unset IFS

# while loop
```
