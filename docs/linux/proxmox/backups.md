# Useful links
<https://pve.proxmox.com/wiki/Backup_and_Restore>  
<https://forum.proxmox.com/threads/extracting-data-from-vma-file.70914/post-318167>  
<https://git.proxmox.com/?p=pve-qemu.git;a=blob_plain;f=vma_spec.txt;hb=refs/heads/master> - VMA Specification  
&nbsp;  
<https://github.com/jancc/vma-extractor>  

# Files
- vma-extractor.py

# Commands
```bash
# Extract QCOW from zst compressed backup
zstd -d -o backup.vma backup.vma.zst
vma extract backup.vma . # or use vma-extractor.py
```

# VMA Extractor
```bash
# Version: Jun 1, 2020
# -------------------------
usage: vma.py [-h] [-v] [-f] [--skip-hash] filename destination
example: ./vma.py backup.vma output-folder/
```