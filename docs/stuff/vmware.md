# Useful links
<https://sanbarrow.com/vmx.html> - VMX parameters wiki  
<https://kb.vmware.com/s/article/1001805> - Available network adapters  
<https://softwareupdate.vmware.com/cds/vmw-desktop/> - CDN installers  
<https://github.com/bytium/vm-host-modules/tree/master> - modules fix #1 (see braches)  
<https://github.com/mkubecek/vmware-host-modules> - modules fix #2 (see braches)  
<https://archive.org/details/vmware-workstation-4.53-to-16.1> - archive.org mirror - OLDver  


# Notes:
```toml
# Ethernet adapter type
ethernet0.virtualDev = "vlance"
ethernet0.virtualDev = "e1000"
ethernet0.virtualDev = "vmxnet"
ethernet0.virtualDev = "vmxnet3"
```
