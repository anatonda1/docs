<https://github.com/mashed-potatoes/PotatoNV> - Kirin bootloader unlock - PotatoNV  
<https://www.netspi.com/blog/technical/mobile-application-penetration-testing/android-root-detection-techniques/> - Root detection  
<https://xdaforums.com/t/no-such-file-or-directory-if-tried-run-binary-file-in-twrp.4201571/#post-86112735> - recovery - No such file or directory in recovery

```bash
# Fastboot A/B partitions
fastboot --set-active=b
fastboot delete-logical-partition product_b 
```
