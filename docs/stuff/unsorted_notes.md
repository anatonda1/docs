# Useful links
<https://github.com/JHRobotics/softgpu> - Virtual GPU in a VM

# X11 via wstunnel on Windows
```dos
C:\cygwin64\bin\run.exe --quote /usr/bin/bash.exe -l -c "cd; /usr/bin/startxwin -- -multiwindow -listen tcp"
.\stuff\tools\wstunnel.exe -L 127.0.0.1:22:127.0.0.1:22 wss://<DST_IP>:443
.\Stuff\tools\kitty\kitty.exe -i path\to\key.ppk -load <PROFILE>
```

# SNMP
- Managers (poll data from agents) + agents (collect data from endpoints)
- agents must be configured to allow managers to access data
- organized in `MIB tree` - predefined structure
- Each node has an `OID` - can be vendor specific
- Latest SNMPv3 included encryption
- Commands:
    - `Get` - manager->agent - requests data, returns a "response" message
    - `Set` - manager->agent - sets a variable, usually configuration
    - `Response` - agent->manager - a reply from agent with collected data
    - `Trap` - agent->manager - asynchronous unsolicited message, may be resent until "inform" is received
    - `Inform` - manager->agent - ack message from manager to agent after a trap

# Telegraf
- <https://docs.influxdata.com/telegraf/v1/plugins/>
- <https://www.influxdata.com/blog/telegraf-best-practices/>
- monitoring tool to gather metrics from a node
- Input plugins - allow gathering
    - Possibility to use pre-formatted files as an input from a different source (use aliases!)
- Output plugins - send the data to desired destination
- Processor plugins - process, modify and filter data
- Aggregator plugins - create aggregate metrics
- Routing
    - via `namepass/namedrop` or `tagpass/tagdrop` params
    - on inputs create an `name_override` param
    - on outputs specify an array of tags with appropriate input overrides (eg. `["test"]`)
- Bucket routing by tag also possible
    - Input - specify `bucket` tag with [inputs.file.tags]
    - Output - specify `bucket_tag` to select a desired bucket
- Sensitive information should be stored in ENV variables
- Resiliency - run multiple instances with different config files
- Commands:
    - `telegraf --config telegraf.conf --test`                                                    # test config file
    - `telegraf config > telegraf.conf`                                                           # generate default config
    - `telegraf --sample-config --input-filter cpu:mem --output-filter influxdb > telegraf.conf`  # generate config template from filters

# InfluxDB
- <https://docs.influxdata.com/influxdb/v1/concepts/storage_engine/> - design and implementation (see storage engine)
- high performance datastore written specifically for time series data
- Uses SQL syntax (InfluxQL), alternatively Flux scripting language
- Mostly `CREATE/READ`, less so `UPDATE/DELETE`
- write and query APIs via HTTP
- Tagging for indexing data
- Ports:
    - `8086` - client-server communication via API
    - `8088` - backup and restore operations
- Config in `/etc/influxdb/influxdb.conf`
- CLI via `influx -precision rfc3339` command
- Lingo:
    - **WAL** / Write Ahead Log - The temporary cache for recently written points, later flushed to TSM
    - **TSM** / Time Structured Merge tree - data storage format
    - **batch** - A collection of data points in InfluxDB line protocol format, separated by newlines
    - **bucket** - each combination of a database and a retention policy (database/retention-policy) represents a bucket
    - **metastore** - contains internal information about the status of the system
    - **shard** - contains the actual encoded and compressed data, and is represented by a TSM file on disk for each block of time.
        - every shard belongs to one and only one shard group.
    - **shard group** - logical containers for shards, organized by time and retention policy
- Each DB has it's own `WAL` and `TSM`
- Default database _internal contains internal runtime metrics
- Primary index is always time, 'tags' and 'fields' are columns
- Format `<measurement>[,<tag-key>=<tag-value>...] <field-key>=<field-value>[,<field2-key>=<field2-value>...] [unix-nano-timestamp]`
- Query examples:
    - `INSERT cpu,host=serverA,region=us_west value=0.64`
    - `INSERT temperature,machine=unit42,type=assembly external=25,internal=37`
    - `SELECT * from /.*/ LIMIT 10`
    - `SELECT * FROM "temperature" LIMIT 50`
- Commands:
    - `curl -G 'http://localhost:8086/query?db=telegraf' --data-urlencode 'q=SELECT mean(usage_idle) FROM cpu'` # fetch data from telegraf database, returns json
- NOTES:
    - UDP also possible, Linux requires some tuning `net.core.rmem_max` or `net.core.rmem_default`

&nbsp;
![influxdb](01_img/influxdb.png)


# Kapacitor
- <https://docs.influxdata.com/kapacitor/v1/introduction/getting-started/>
- data processing tool
- uses concept of `topics`, `subscriptions` and `handlers` (consumers)
- allows integration with alerting systems (pagerduty, slack, etc.)
- can pull data from InfluxDB using subscriptions (stream tasks) - <https://docs.influxdata.com/kapacitor/v1/administration/subscription-management/>
- Subscriptions IDs are stored in `/var/lib/kapacitor/`
- HTTP API port `9092` (same as kafka)
- uses TOML for configuration
- uses TICKscript to work on data, called tasks
    - starts with `dbrp` (db / retention policy) stanza at the beggining
    - followed by the task type (`stream` / `batch`) and configuration (`from()`, `query()`, `eval()`, `window()`, `alert()`, etc.)
- Task types:
    - `stream` - replicates data from influxdb locally, used for offloading
    - `batch` - query and process data for a specified interval
- Commands:
    - `tail -f -n 128 /var/log/kapacitor/kapacitor.log`
    - `kapacitor stats general`
    - `kapacitor list tasks`
    - `kapacitor show cpu_alert`
    - `kapacitor define cpu_alert -tick cpu_alert.tick`
    - `kapacitor enable cpu_alert`
    - `kapacitor define-topic-handler ./slack.yaml` (also used for updating the configuration)
    - `kapacitor show-topic cpu`
- **Notes**:
    - TICKscript - Single quotes for strings, double quotes for fields
    - Good idea to test the task before enabling it
        - <https://docs.influxdata.com/kapacitor/v1/introduction/getting-started/#test-the-task>
        - configure alert to write to a log file first via `alert().log()`
        - `kapacitor record stream -task cpu_alert -duration 60s` # returns record ID
        - `kapacitor list recordings $rid`
        - `kapacitor replay -recording $rid -task cpu_alert`
        - `cat /tmp/alerts.log`

# Kafka
- Links:
    - <https://developer.confluent.io/what-is-apache-kafka/>
    - <https://www.cloudkarafka.com/blog/cloudkarafka-what-is-zookeeper.html>
    - <https://www.root.cz/clanky/apache-kafka-distribuovana-streamovaci-platforma/>
    - <https://www.root.cz/clanky/sledovani-cinnosti-systemu-apache-kafka-pres-jmx-i-metriky-promethea/>
    - <https://www.root.cz/clanky/sledovani-cinnosti-systemu-apache-kafka-pres-jmx-i-metriky-promethea-dokonceni/>
    - <https://www.root.cz/clanky/kafka-connect-tvorba-producentu-a-konzumentu-bez-zdrojoveho-kodu/>
    - <https://www.root.cz/clanky/kafka-connect-definice-a-kontrola-schematu-zprav/>
    - <https://www.root.cz/clanky/temata-oddily-a-replikace-v-systemu-apache-kafka/>
    - <https://www.root.cz/clanky/nastroj-mirrormaker-pro-apache-kafku/>
- High availabiliry, scalable distributed message broker
- Custom binary protocol over TCP
- uses zookeeper as a configuration and coordination backend - can be clustered
- Collects events (notification + state [usually JSON]) as KV pairs
- Brokers
    - Brokers also responsible for replication of partitions
    - Leader + Follower design, messages in general written to the leader and then distributed to followers
- Producers - endpoint with API access to the kafka cluster, usually via a library
- Consumers - client with API access which subscribes to topics, many consumers can access a single topic
- Topics - append-only logs
    - Reading - `kafka-console-consumer.sh --topic quickstart-events --from-beginning --bootstrap-server localhost:9092`
    - Writing - `kafka-console-producer.sh --topic quickstart-events --bootstrap-server localhost:9092`
    - Listing - `kafka-topics.sh --list --bootstrap-server localhost:9092 (--zookeeper localhost:2181)`
    - Details - `kafka-topics.sh --describe --bootstrap-server localhost:9092`
    - Topic reconfiguration - `kafka-reassign-partitions.sh --topics-to-move-json-file topics.json --execute`
- Partitioning - splitting topics between cluster nodes
    - unkeyed messages are round-robin, possibly out of order
    - keyed messages stay within the same partition (using key hash)
- **EXTRAS**:
    - Kafka Connect - module-based connector for integration with other systems
    - Schema registry - API for data schemas
- Cluster notes:
    - Ports:
        - `2181` - zookeeper
        - `9092` - kafka
    - hosts file! or set `listeners=PLAINTEXT://IPADDR:9092` in `server.properties`, otherwise uses hostname
    - zookeeper requires some additional params - <https://www.clairvoyant.ai/blog/kafka-series-3.-creating-3-node-kafka-cluster-on-virtual-box>
    - beware of data dirs in `/tmp` - <https://serverfault.com/questions/997762/unable-to-start-kafka-with-zookeeper-kafka-common-inconsistentclusteridexceptio>

# Prometheus
- PromQL
- Thanos for clustering/HA

# Grafana
- <https://grafana.com/docs/grafana/latest/panels-visualizations/query-transform-data/expression-queries/>
- queries - can be visual vs query editor
- expressions - optional additional server side post processing (done on grafana)
- queries depend on the underlying metrcis backend

# Logstash
- Log aggregator and processor
- Data pipeline with input/filter/output functionality
- one does not use logstash on it's own but rather in conjunction with data storage and presentation
- uses input plugins to ingest the data from various sources

# Zookeeper + component design
![zkcomp](01_img/zookeeper_comps.png)
