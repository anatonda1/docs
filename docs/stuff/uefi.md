# Useful links
<https://depletionmode.com/uefi-boot.html>  
<https://oofhours.com/2019/09/02/geeking-out-with-uefi/>  
<https://oofhours.com/2020/11/25/geeking-out-with-uefi-again/>  
<https://wiki.lfedge.org/display/EVE/Measured+Boot+and+Remote+Attestation#MeasuredBootandRemoteAttestation-MeasuredBootvsSecureBoot>  
<https://techcommunity.microsoft.com/t5/windows-it-pro-blog/updating-microsoft-secure-boot-keys/ba-p/4055324> - MS secure boot update  


# Overview
![UEFI](01_img/uefi.png)

# Boot guard
CDI (Constrained Data Item) - trusted data  
UDI (Unconstrained Data Item) - untrusted data  
TP (Transformation Procedure) - the procedure that will be applied to UDI to turn it into CDI; such as by certifying it with an IVP (Integrity Verification Procedure)  

- The CSME starts execution from the reset vector of its ROM code. The ROM is assumed to be CDI from the start and hence is the Root-of-Trust
- The initial parts of the CSME firmware are loaded off the flash into SRAM (UDI), verified by the ROM (now becoming CDI) and executed
- The CPU uCode (microcode) will be loaded. This uCode is considered UDI but is verified by the CPU ROM which acts as the Root-of-Trust for the CPU
- Boot Guard is enabled, so the uCode will load a component called the ACM (Authenticated Code Module) (UDI) off the flash, and will, using the CPU signing key (fused into the CPU), verify it
- The ACM (now CDI) will request the hash of the OEM IBB signing key from the CSME. The CSME is required here as it has access to the FPF (Field Programmable Fuses) which are burned by the OEM at manufacturing time
- The ACM will load the IBB (UDI) and verify it using the OEM key (now CDI).


# EFI Shell Commands
```bash
# Print all variables
dmpstore -all -b

# Print all variables (CSV formatted) to variables.txt
dmpstore -all -sfo > fs0:\variables.txt

```
