# Useful links
<https://www.moritz.systems/blog/before-the-bsd-kernel-starts-part-one-on-amd64/>  
<https://superuser.com/questions/988473/why-is-the-first-bios-instruction-located-at-0xfffffff0-top-of-ram>

# Bootstrap process
**NOTE:** Different architectures use different boot processes, this process only applies to x86-compatible archs

- CPU starts
- Jump to address 0xFFFFFFF0 (hardcoded) at which the first instruction of BIOS ROM is addressed
    - Note: Addressing and ammount of available physical RAM does not neccessarily correlate, it is possible to have BIOS Flash addressed at high address values even if RAM < 4GB
- CPU and memory initialization + BIOS is copied from ROM to RAM (lower 1MB 0xF0000 - 0xFFFFF)
- BIOS reads the 512 bytes long MBR from disk device (region must end with 0xAA55 signature) and copies it to RAM at address 0x07C00 
- BIOS jumps to 0x07C00 and executres MBR
- MBR initializes and starts the bootloader (grub, lilo, windows bootloader ...)
- OS kernel starts...

&nbsp;
![BIOS](01_img/bios.png)