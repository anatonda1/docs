# Calculator
<https://www.blackfiveservices.co.uk/fanspeedcalc.php?Voltage=12&Current=2.2&Target=9>

# DL180
<https://imgur.com/a/ydJaD>

# DL380 G7
<https://www.reddit.com/r/homelab/comments/4aymj8/dl380_g7_fan_noise_reduction_success/>  
<https://www.reddit.com/r/homelab/comments/72k3jf/faking_the_fan_signal_on_a_dl380_g7/>  
<https://www.reddit.com/r/homelab/comments/6xfscd/making_the_dl380_g7_quiet_for_good/>  

# Via arduino
<https://homeservershow.com/forums/topic/7294-faking-the-fan-signal/?tab=comments#comment-79985>  
<https://github.com/Max-Sum/HP-fan-proxy>