# Useful links
<https://support.hpe.com/hpesc/public/swd/detail?swItemId=MTX_4f842ceb31cf48d392e22705a8>  
<http://www.it-fricke.net/it/hp_ilo.html>


# Files
- HPLOCONS.exe

# Notes
Standalone GUI Remote Console application for HP ILO  

## Proxy notes
When accessing remote console via a jump/proxy server, following ports must be enabled:  

- `17990` for remote console
- `17988` for virtual media

For local port forwarding, configure putty/kitty/ssh as following:  
`L8081:<ILO-IPADDR>:443`  
`L17990:<ILO-IPADDR>:17990`  
`L17988:<ILO-IPADDR>:17988`  

**Note:** Only a single remote console instance is possible using local port forward  

# Commands
```powershell
# To run via command line
HPLOCONS.exe -addr "IP-Address" -name Administrator -password "ilo-password" -lang en
```
