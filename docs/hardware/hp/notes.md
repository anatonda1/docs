# Useful links
<https://www.digiboy.ir/?s=Service+Pack+For+Proliant> - SPP download


# Notes
## Boot from a Backup ROM
<https://community.hpe.com/t5/ProLiant-Servers-ML-DL-SL/How-to-boot-DL385-with-the-Backup-ROM/td-p/3867700>  

To access the redundant ROM through RBSU:  

1. Access `RBSU` by pressing the `F9` key during powerup when the prompt is displayed in the upper right corner of the screen
2. Select `Advanced Options`
3. Select `Redundant ROM Selection`
4. Select the ROM version
5. Press the `Enter` key
6. Press the `Esc` key to exit the current menu or press the `F10` key to exit `RBSU`. The server restarts automatically

&nbsp;  
To access the redundant ROM manually:  

1. Power down the server
2. Remove the access panel
3. Set positions 1, 5, and 6 of the system maintenance switch to On
4. Install the access panel
5. Power up the server
6. Wait for the server to emit two beeps
7. Repeat steps 1 and 2
8. Set positions 1, 5, and 6 of the system maintenance switch to Off
9. Repeat steps 4 and 5