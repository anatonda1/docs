# Root on USB device
- Write the latest Raspbian image to an SD card and boot it.
- Write the latest Raspbian image to a USB drive and plug it into the Pi.
- Run `blkid` to determine the `PARTUUID` of the USB drive.
- Edit `/boot/cmdline.txt` and change `root=PARTUUID=xxxxxxxx` to match the `PARTUUID` of the USB drive.
- Reboot. The Pi should be running from the USB drive.

## Example:
```bash
console=serial0,115200 console=tty1 root=PARTUUID=437e266a-02 rootfstype=ext4 fsck.repair=yes rootwait
```
