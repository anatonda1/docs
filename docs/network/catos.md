# Useful links
Firmware download (Cisco account needed):  
<https://software.cisco.com/download/release.html?mdfid=279486488&softwareid=280805680>  
<https://software.cisco.com/download/release.html?mdfid=279486488&softwareid=280805680&release=12.2.55-SE12&relind=AVAILABLE&rellifecycle=MD&reltype=latest>

# Commands
## Reset to factory defaults

```cisco
# 1. Remove current config
# ========================================
enable
write erase
reload

System configuration has been modified. Save? [yes/no]: n

# Do not save the configuration at this prompt, otherwise the switch reloads with the current running configuration and does not reset to default.

Proceed with reload? [confirm] y

# 2. Remove VLANs (if any)
# ========================================
# Check for existing VLANs
show vlan

# Remove and reload
dir:flash
flash:vlan.dat
reload
```

## IP Address setup
```cisco
config t
interface vlan 1
ip address 192.168.1.100 255.255.255.0
no shutdown
end
```

## Jumbo frames
Jumbo frames are used automatically for all gigabitEthernet ports  
Setting system-wide Jumbo MTU to 9000 will automatically set MTU to 9000 for GigabitEthernet ports only!  
It's not possible to configure MTU on a Catalyst 3750 for a single port  

**Note:** MTU settings is not stored in global switch configuration but inside the switches' NVRAM, therefore it must be set-up manually for each new switch even when copying an old configuration  
<https://murison.wordpress.com/2014/02/13/cisco-jumbo-frame-setting/>


```cisco
# Check the current status
enable
show system mtu

System MTU size is 1500 bytes
System Jumbo MTU size is 1500 bytes
System Alternate MTU size is 1500 bytes
Routing MTU size is 1500 bytes

# Set Jumbo MTU to 9000
config t
system mtu jumbo 9000
write
reload
```

## Stack Mode
Stack mode firmware update  
<https://www.cisco.com/c/en/us/support/docs/switches/catalyst-3750-series-switches/64898-upgrade-3750-stack.html>

``` cisco
# Change master switch
# ========================================
show switch

Switch#  Role      Mac Address     Priority     State
--------------------------------------------------------
 1       Slave     0016.4748.dc80     1         Ready
*2       Master    0016.9d59.db00     5         Ready
	
# NOTE: Master has always highest priority:

enable
conf t
switch <Switch#> priority <number>
write memory
reload

# Recheck:
show switch

Switch#  Role      Mac Address     Priority     State
--------------------------------------------------------
*1       Master     0016.4748.dc80     6         Ready
 2       Slave      0016.9d59.db00     5         Ready

```

## LACP Setup
<https://www.cisco.com/c/en/us/support/docs/lan-switching/etherchannel/12023-4.html#topic1>  
<https://supportforums.cisco.com/t5/lan-switching-and-routing/lacp-load-balancing-method/td-p/2445171>  
<http://www.mustbegeek.com/configure-lacp-etherchannel-in-cisco-ios-switch/>  

```cisco
# 1. Create LAG
# ========================================
# Configure physical ports
enable
config t
interface giX/X/XX
# or use "interface range giX/X/XX-XX"

channel-protocol lacp
channel-group X mode active

# Configure channel groups, switchport options automatically propagate to physical ports
interface port-channel X
switchport trunk encapsulation dot1q
switchport mode trunk

# Check
show interfaces trunk

# Save
write

# NOTES:
# X - Channel group number
# Channel group number is used to determine which ports are assigned to a certain LAG
# switchport trunk is not needed if all interfaces in each LAG are on the same VLAN - switchport mode defaults to "access"

# 2. Configure balance policy
# ========================================
enable
config t
port-channel load-balance
	○ dst-ip       Dst IP Addr
	○ dst-mac      Dst Mac Addr
	○ src-dst-ip   Src XOR Dst IP Addr
	○ src-dst-mac  Src XOR Dst Mac Addr
	○ src-ip       Src IP Addr
	○ src-mac      Src Mac Addr

# NOTE: Select "src-dst-ip" or "src-dst-mac"
```

## VLANs setup
```cisco
# Configure trunk on ports which should support VLANs
enable
config t
interface range Gi1/0/1 - 3
switchport trunk encapsulation dot1q
switchport mode trunk
end

# Create a VLAN
enable
config t
vlan 100
name DMZ
end

# Check
enable
show interfaces trunk
show vlan

# Save config
write
```